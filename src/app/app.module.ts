import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MaterialModule } from './material';
import { AccueilComponent } from './accueil/accueil.component';
import { RouterModule, Routes } from '@angular/router';
import { ExpertComponent } from './expert/expert.component';
import { ContactComponent } from './contact/contact.component';

const appRoutes: Routes = [
  { path: 'accueil', component: AccueilComponent},
  { path: 'expert', component: ExpertComponent},
  { path: 'contact', component: ContactComponent},
  { path: '', component: AccueilComponent},
  { path: '**', component: AccueilComponent}
]



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AccueilComponent,
    ExpertComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {




}
